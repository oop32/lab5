package com.pharadon.lab5;

import static org.junit.Assert.assertArrayEquals;

import org.junit.Test;

public class BubbleSortAppTest {
    @Test
    public void ShouldBubbleTestcase1() {
        int arr[] = { 5, 4, 3, 2, 1 };
        int expected[] = { 4, 3, 2, 1, 5 };
        int first = 0;
        int second = 4;
        BubbleSortApp.bubble(arr, first, second);
        assertArrayEquals(expected, arr);
    }

    @Test
    public void ShouldBubbleTestcase2() {
        int arr[] = { 4, 3, 2, 1, 5 };
        int expected[] = { 3, 2, 1, 4, 5 };
        int first = 0;
        int second = 3;
        BubbleSortApp.bubble(arr, first, second);
        assertArrayEquals(expected, arr);
    }

    @Test
    public void ShouldBubbleTestcase3() {
        int arr[] = { 3, 2, 1, 4, 5 };
        int expected[] = { 2, 1, 3, 4, 5 };
        int first = 0;
        int second = 2;
        BubbleSortApp.bubble(arr, first, second);
        assertArrayEquals(expected, arr);
    }

    @Test
    public void ShouldBubbleTestcase4() {
        int arr[] = { 2, 1, 3, 4, 5 };
        int expected[] = { 1, 2, 3, 4, 5 };
        int first = 0;
        int second = 1;
        BubbleSortApp.bubble(arr, first, second);
        assertArrayEquals(expected, arr);
    }

    @Test
    public void shouldBubbleSortTestCase1() {
        int arr[] = { 5, 4, 3, 2, 1 };
        int expected[] = { 1, 2, 3, 4, 5 };
        BubbleSortApp.bubblesort(arr);
        assertArrayEquals(expected, arr);
    }

    @Test
    public void ShouldBubbleSortcase2() {
        int arr[] = { 4, 3, 2, 1, 5 };
        int expected[] = { 1,2,3,4,5 };
        BubbleSortApp.bubblesort(arr);
        assertArrayEquals(expected, arr);
    }

    @Test
    public void ShouldBubbleSortcase3() {
        int arr[] = { 3, 2, 1, 4, 5 };
        int expected[] = { 1,2,3,4,5 };
        BubbleSortApp.bubblesort(arr);
        assertArrayEquals(expected, arr);
    }

    @Test
    public void ShouldBubbleSortcase4() {
        int arr[] = { 2, 1, 3, 4, 5 };
        int expected[] = { 1, 2, 3, 4, 5 };
        BubbleSortApp.bubblesort(arr);
        assertArrayEquals(expected, arr);
    }

}
