package com.pharadon.lab5;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class SelectionSortAppTest {
    @Test
    public void shouldFindMinIndexTestCase1(){
        int arr[] = {5,4,3,2,1};
        int pos = 0;
        int minIndex = SelectionSortApp.findMinIndex(arr,pos); //arr pos คือการinput
        assertEquals(4, minIndex);
    }
    @Test
    public void shouldFindMinIndexTestCase2(){
        int arr[] = {1,4,3,2,5};
        int pos = 1;
        int minIndex = SelectionSortApp.findMinIndex(arr,pos); //arr pos คือการinput
        assertEquals(3, minIndex);
    }

    @Test
    public void shouldFindMinIndexTestCase3(){
        int arr[] = {1,2,3,4,5};
        int pos = 2;
        int minIndex = SelectionSortApp.findMinIndex(arr,pos); //arr pos คือการinput
        assertEquals(2, minIndex);
    }

    @Test
    public void shouldFindMinIndexTestCase4(){
        int arr[] = {1,1,1,1,1,0,1,1};
        int pos = 0;
        int minIndex = SelectionSortApp.findMinIndex(arr,pos); //arr pos คือการinput
        assertEquals(5, minIndex);
    }

    @Test
    public void shouldswaptestcase1(){
        int arr[] = {5,4,3,2,1};
        int expected[] = {1,4,3,2,5};
        int first = 0;
        int second = 4;
        SelectionSortApp.swap(arr, first, second);
        assertArrayEquals(expected,arr);
    }

    @Test
    public void shouldswaptestcase2(){
        int arr[] = {5,4,3,2,1};
        int expected[] = {5,4,2,3,1};
        int first = 2;
        int second = 3;
        SelectionSortApp.swap(arr, first, second);
        assertArrayEquals(expected,arr);
    }

    @Test
    public void shouldSelectionSortTestCase1() {
        int arr[] = {5,4,3,2,1};
        int sortedarr[] = {1,2,3,4,5};
        SelectionSortApp.SelectionSort(arr);
        assertArrayEquals(sortedarr, arr);

    }

    @Test
    public void shouldSelectionSortTestCase2() {
        int arr[] = {10,9,8,7,6,5,4,3,2,1};
        int sortedarr[] = {1,2,3,4,5,6,7,8,9,10};
        SelectionSortApp.SelectionSort(arr);
        assertArrayEquals(sortedarr, arr);

    }

    @Test
    public void shouldSelectionSortTestCase3() {
        int arr[] = {5,6,7,1,3,7};
        int sortedarr[] = {1,3,5,6,7,7};
        SelectionSortApp.SelectionSort(arr);
        assertArrayEquals(sortedarr, arr);

    }



}
